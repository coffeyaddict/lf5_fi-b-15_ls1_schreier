
public class VariablenUse {
  
	public static void main(String [] args){
    
		/* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
			int zaehler = 25;
    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
			System.out.println(zaehler);
    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
			char menuepunkt = 'C';
    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
			System.out.println(menuepunkt);
    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
			int lightspeed = 299792458;
    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
			System.out.println("Wert der Lichtgeschwindigkeit in m/s: " + lightspeed);
    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
			byte vereinsverwaltungMitglieder = 7;
    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
			System.out.println("Anzahl der Mitglieder innerhalb des Vereins: " + vereinsverwaltungMitglieder);
    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
			double elementarladung = 1.602177;
			System.out.println("Wert der Elementarladung: " + elementarladung + " * 10-19 C");

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
			boolean ZahlungIstErfolgt = true;
    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
			System.out.println("Die Zahlung ist erfolgt: " + ZahlungIstErfolgt);
  }//main
}// Variablen