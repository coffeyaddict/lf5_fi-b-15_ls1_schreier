
public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 100000000000l;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3664088;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 6868;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 200000;  
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 357376;
    
    // Wie gro� ist das kleinste Land der Erde? (in m�)
    
    int flaecheKleinsteLand = 440000;
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne in unserem Sonnensystem: " + anzahlSterne);
    
    System.out.println("Anzahl der Einwohner in Berlin: " + bewohnerBerlin);
    
    System.out.println("Mein pers�nliches Alter in Tage: " + alterTage);
    
    System.out.println("Gewicht des schwersten Tieres der Welt in kilogramm: " + gewichtKilogramm);
    
    System.out.println("Fl�che des gr��ten Landes der Erde in km�: " + flaecheGroessteLand);
    
    System.out.println("Fl�che des kleinsten Landes der Erde in m�: " + flaecheKleinsteLand);
    
    System.out.println(" *******  Ende des Programms  ******* ");
    System.out.println();
  }
}
