﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		double rueckgabebetrag;

		while (true) {

			zuZahlenderBetrag = fahrkartenbestellungErfassen();

			// Geldeinwurf
			// -----------

			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			// Fahrscheinausgabe
			// -----------------

			fahrkartenAusgeben();

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------

			rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
			if (rueckgabebetrag > 0.0) {
				rueckgeldAusgeben(rueckgabebetrag);
			}

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.");
		}
	}

	// --------------------------------------------------------------------------------------------------------------------------//

	public static double fahrkartenbestellungErfassen() {

		double preisTickets = 0;
		int anzahlTickets = 0;
		int ticketAuswahl = 0;
		boolean bestellungAbschluss = false;
		boolean korrekteTicketauswahl;
		double zuZahlenderBetrag = 0;
		Scanner tastatur = new Scanner(System.in);
		
		while (bestellungAbschluss == false) {
			korrekteTicketauswahl = false;
			
			if (zuZahlenderBetrag > 0) {
				System.out.println("\nWeitere Tickets kaufen?");
			}
		
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: ");
		System.out.println("");
		System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
		System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
		System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		System.out.println("Bezahlen (9)");
		System.out.println("");
		
		while (!korrekteTicketauswahl) {
			
			System.out.print("Ihre Auswahl: ");
			
			korrekteTicketauswahl = true;
			ticketAuswahl = tastatur.nextInt();
			if (ticketAuswahl == 1) {
				preisTickets = 2.90;
				break;
			}
			if (ticketAuswahl == 2) {
				preisTickets = 8.60;
				break;
			}
			if (ticketAuswahl == 3) {
				preisTickets = 23.50;
				break;
			}
			if (ticketAuswahl == 9) {
				bestellungAbschluss = true;
				break;	
			}
			 
		}
		
	
		if (!bestellungAbschluss) {
			
			do {
		

				System.out.print("Anzahl der Tickets (maximal 10 Tickets): ");
				anzahlTickets = tastatur.nextInt();

				if (anzahlTickets > 10 || anzahlTickets < 1) {
					System.out.println(" >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
				}
			} while (anzahlTickets > 10 || anzahlTickets < 1);

			zuZahlenderBetrag = zuZahlenderBetrag + preisTickets * anzahlTickets;
		}
		}
	System.out.printf("Gesamtbetrag (EURO): %.2f\n\n", zuZahlenderBetrag);
	return zuZahlenderBetrag;

}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");

		warte(250);

		System.out.println("\n\n");

	}

	public static void warte(int millisekunde) {

		for (int i = 0; i < 16; i++) {
			System.out.print("=");
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				e.printStackTrace();

			}

		}
	}

	public static void rueckgeldAusgeben(double rueckgabebetrag) {
		System.out.printf("Der Rückgabebetrag in einer Höhe von %.2f EURO\n", rueckgabebetrag);
		System.out.println("wird wie folgt ausgezahlt (IN MÜNZEN):");

		while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
		{
			muenzeAusgeben(2, "EURO");
			rueckgabebetrag -= 2.0;
		}
		while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
		{
			muenzeAusgeben(1, "EURO");
			rueckgabebetrag -= 1.0;
		}
		while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
		{
			muenzeAusgeben(50, "CENT");
			rueckgabebetrag -= 0.5;
		}
		while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
		{
			muenzeAusgeben(20, "CENT");
			rueckgabebetrag -= 0.2;
		}
		while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
		{
			muenzeAusgeben(10, "CENT");
			rueckgabebetrag -= 0.1;
		}
		while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
		{
			muenzeAusgeben(5, "CENT");
			rueckgabebetrag -= 0.05;
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}
}

// 2.5 Aufgabe 5
// Ein int Wert ergibt hier am meisten Sinn, da man ja keine geteilten
// Fahrkarten kaufen kann.
// Ein double Wert habe ich beim Fahrkartenpreis deshalb genommen, da beim
// Fahrkartenkauf auch Komma-Werte möglich sind.
// 2.5 Aufgabe 6
// In meinem Beispiel wird der int Wert für anzahlFahrkarten und der double Wert
// für preisEinzelfahrkarte miteinander multipliziert,
// um dann den Gesamtpreis (zuZahlenderBetrag) rauszubekommen.