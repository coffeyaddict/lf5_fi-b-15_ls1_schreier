
public class Aufgabe2Multiplikation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		double x = 2.36;
		double y = 7.87;
		
		
		double ergebnis = verarbeitung(x, y);
		
		ausgabe (x, y, ergebnis);
		
	}
		
	public static double verarbeitung(double x, double y) {
		double ergebnis= x * y;
		return ergebnis;
		
	}
	public static void ausgabe(double x,double y, double ergebnis) {
		System.out.printf("\nx = %.2f * y = %.2f = ergebnis = %.2f\n ", x, y, ergebnis);
	}
}
