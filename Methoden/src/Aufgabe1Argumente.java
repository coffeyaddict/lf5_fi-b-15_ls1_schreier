
public class Aufgabe1Argumente {
	public static void main(String[] args) {
			ausgabe(1, "Mana");
			ausgabe(2, "Elise");
			ausgabe(3, "Johanna");
			ausgabe(4, "Felizitas");
			ausgabe(5, "Karla");
			System.out.println(vergleichen(1, 2));
			System.out.println(vergleichen(1, 5));
			System.out.println(vergleichen(3, 4));

	}

	
	public static void ausgabe(int zahl, String name) {
		System.out.println(zahl + ": " + name);
	}

	public static boolean vergleichen(int arg1, int arg2) {
		return (arg1 + 8) < (arg2 * 3);
	}
}

// Meine ersten Gedanken:
// In der ersten Klasse sieht man die verschiedenen Ausgaben, welche darunter miteinander verglichen werden sollen.
// In der zweiten Klasse werden als erstes die einzelnen Namen mit der jeweiligen davor stehenden Zahl ausgegeben.
// In der letzten Klasse werden die verschiedenen Zahlenpaarungen miteinander verglichen bzw. wird der erste Zahlenwert mit 8 addiert und der zweite Zahlenwert mit 3 multipliziert.

//Ergebnis:
//Das Programm vergleicht die 3 Zahlenpaarungen miteinander. 
//Beispiel Zahlenpaar 1,2: 1 wird mit 8 addiert, 2 mit 3 multipliziert.
//wenn das "kleiner als" erf�llt ist gibt das Programm ein true aus. Wenn nicht, dann ein false.
//In dem Beispiel 1,2 kommt ein false raus, da das Ergebnis "9 < 6" lautet.
