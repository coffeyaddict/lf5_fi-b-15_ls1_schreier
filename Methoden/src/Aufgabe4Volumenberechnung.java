import java.util.Scanner;

public class Aufgabe4Volumenberechnung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Bitte geben sie alle Angaben in cm an!");
		System.out.println("W�rfel: " + wuerfel(eingabeWuerfel()) + "cm�");
		System.out.println("Quader: " + quader(eingabe("Quader: L�nge der ersten Seite angeben"),
																					eingabe("Quader: L�nge der zweiten Seite angeben"),
																					eingabe("Quader: L�nge der dritten Seite angeben"))
																	+ "cm�");
		
		System.out.println("Pyramde: " + pyramide(eingabePyramideL(), eingabePyramideH()) + "cm�");
		System.out.println("Kugel: " + kugel(eingabeKugel()) + "cm�");
	}
		
	public static double rechnung(double x, double y) {
			
			double ergebnis = x * y;
			return ergebnis;
			
	}
		
	public static double wuerfel(double a) {	 
		
		
		double v = a * a * a;
		return v;
		
	}
		
	public static double eingabeWuerfel() {	
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("W�rfel: L�nge einer Seite angeben");
		double x = myScanner.nextDouble();
		return x;
		
	}
		
	public static double quader(double a, double b, double c) {
		
		double v = a * b * c;
		return v;
	}
	
	public static double eingabe(String text) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double x = myScanner.nextDouble();
		return x;
		
	}
		
	public static double pyramide(double a, double h) {
		
		double f = h / 3;
		double v = a * a * f;
		return v;
	}
	
	public static double eingabePyramideL() {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Pyramide: L�nge angeben");
		double x = myScanner.nextDouble();
		return x;
		
	}
	
	public static double eingabePyramideH() {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Pyramide: H�he angeben");
		double x = myScanner.nextDouble();
		return x;
		
	}
	
	public static double kugel(double r) {
		
		double f = (double) 4 / (double) 3;
		double g = r * r * r;
		double v = f * g * Math.PI;
		return v;
	}
	
	public static double eingabeKugel() {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Kugel: Radius eingeben");
		double x = myScanner.nextDouble();
		return x;
	}
}
