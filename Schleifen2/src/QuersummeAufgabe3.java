import java.util.Scanner;

public class QuersummeAufgabe3 {

	public static void main(String[] args) {
	
		Scanner tastatur = new Scanner (System.in);
		System.out.print("Bitte geben sie eine beliebige ganze Zahl ein: ");
		int eingabewert = tastatur.nextInt();
		int quersumme = 0;
		
		while (eingabewert > 0) {
			quersumme = quersumme + eingabewert%10;
			eingabewert = eingabewert / 10;
		}
		System.out.println("Die Quersumme der eingegebenen Zahl lautet: " + quersumme );
		}

	}
