import java.util.Scanner;

public class JavaTrain {

	public static void main(String[] args) {
		int fahrzeit = 0;
		char haltInSpandau = 'n';
		char richtungHamburg = 'n';
		char haltInStendal = 'n';
		char endetIn = 'h';

		fahrzeit = fahrzeit + 8; // Fahrzeit: Berlin Hbf -> Spandau

		if (haltInSpandau == 'j') {
			fahrzeit = fahrzeit + 2; // Halt in Spandau
		}

		if (richtungHamburg == 'j') {
			fahrzeit = fahrzeit + 96; // Zug f�hrt Richtung Hamburg
			System.out.println("Sie erreichen Hamburg nach " + fahrzeit + " Minuten");
		} else {
			fahrzeit = fahrzeit + 34; // Zug f�hrt Richtung Stendal

			if (haltInStendal == 'j') {
				fahrzeit = fahrzeit + 16; // Zug f�hrt �ber Stendal Hbf
			} else {
				fahrzeit = fahrzeit + 6; // Zug umf�hrt Stendal
			}
			if (endetIn == 'h') {
				fahrzeit = fahrzeit + 62; // Zug endet in Hannover
				System.out.println("Sie erreichen Hannover nach " + fahrzeit + " Minuten");
			} else if (endetIn == 'w') {
				fahrzeit = fahrzeit + 29; // Zug endet in Wolfsburg
				System.out.println("Sie erreichen Wolfsburg nach " + fahrzeit + " Minuten");
			} else {
				fahrzeit = fahrzeit + 50; // Zug endet in Braunschweig
				System.out.println("Sie erreichen Braunschweig nach " + fahrzeit + " Minuten");
			}
		}
	}
}