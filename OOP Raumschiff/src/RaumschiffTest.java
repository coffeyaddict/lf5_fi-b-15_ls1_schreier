
public class RaumschiffTest {

	public static void main(String[] args) {

		// Raumschiffe

		Raumschiff r1klingonen = new Raumschiff();
		Raumschiff r2romulaner = new Raumschiff();
		Raumschiff r3vulkanier = new Raumschiff();

		r1klingonen.setSchiffsname("IKS Hegh`ta");
		r1klingonen.setAndroidenAnzahl(2);
		r1klingonen.setLebenserhaltungssystemeInProzent(100);
		r1klingonen.setHuelleInProzent(100);
		r1klingonen.setSchildeInProzent(100);
		r1klingonen.setEnergieversorgungInProzent(100);
		r1klingonen.setPhotonentorpedoAnzahl(1);

		r2romulaner.setSchiffsname("IRWKhazara");
		r2romulaner.setAndroidenAnzahl(2);
		r2romulaner.setLebenserhaltungssystemeInProzent(100);
		r2romulaner.setHuelleInProzent(100);
		r2romulaner.setSchildeInProzent(100);
		r2romulaner.setEnergieversorgungInProzent(100);
		r2romulaner.setPhotonentorpedoAnzahl(2);

		r3vulkanier.setSchiffsname("Ni`Var");
		r3vulkanier.setAndroidenAnzahl(5);
		r3vulkanier.setLebenserhaltungssystemeInProzent(100);
		r3vulkanier.setHuelleInProzent(100);
		r3vulkanier.setSchildeInProzent(100);
		r3vulkanier.setEnergieversorgungInProzent(100);
		r3vulkanier.setPhotonentorpedoAnzahl(0);

		System.out.println("Name des Schiffs: " + r1klingonen.getSchiffsname());
		System.out.println("Androidenanzahl auf dem Schiff: " + r1klingonen.getAndroidenAnzahl());
		System.out.println("Lebenserhaltungssysteme in Prozent: " + r1klingonen.getLebenserhaltungssystemeInProzent());
		System.out.println("St�rke der Schiffsh�lle in Prozent: " + r1klingonen.getHuelleInProzent());
		System.out.println("St�rke der Schilde in Prozent: " + r1klingonen.getSchildeInProzent());
		System.out.println("Energieversorgung in Prozent: " + r1klingonen.getEnergieversorgungInProzent());
		System.out.println("Anzahl der Photonentorpedos auf dem Schiff: " + r1klingonen.getPhotonentorpedoAnzahl());
		System.out.println();

		System.out.println("Name des Schiffs: " + r2romulaner.getSchiffsname());
		System.out.println("Androidenanzahl auf dem Schiff: " + r2romulaner.getAndroidenAnzahl());
		System.out.println("Lebenserhaltungssysteme in Prozent: " + r2romulaner.getLebenserhaltungssystemeInProzent());
		System.out.println("St�rke der Schiffsh�lle in Prozent: " + r2romulaner.getHuelleInProzent());
		System.out.println("St�rke der Schilde in Prozent: " + r2romulaner.getSchildeInProzent());
		System.out.println("Energieversorgung in Prozent: " + r2romulaner.getEnergieversorgungInProzent());
		System.out.println("Anzahl der Photonentorpedos auf dem Schiff: " + r2romulaner.getPhotonentorpedoAnzahl());
		System.out.println();

		System.out.println("Name des Schiffs: " + r3vulkanier.getSchiffsname());
		System.out.println("Androidenanzahl auf dem Schiff: " + r3vulkanier.getAndroidenAnzahl());
		System.out.println("Lebenserhaltungssysteme in Prozent: " + r3vulkanier.getLebenserhaltungssystemeInProzent());
		System.out.println("St�rke der Schiffsh�lle in Prozent: " + r3vulkanier.getHuelleInProzent());
		System.out.println("St�rke der Schilde in Prozent: " + r3vulkanier.getSchildeInProzent());
		System.out.println("Energieversorgung in Prozent: " + r3vulkanier.getEnergieversorgungInProzent());
		System.out.println("Anzahl der Photonentorpedos auf dem Schiff: " + r3vulkanier.getPhotonentorpedoAnzahl());
		System.out.println();

		// Ladungen
		Ladung FerengiSchneckensaft = new Ladung();
		Ladung BorgSchrott = new Ladung();
		Ladung RoteMaterie = new Ladung();
		Ladung Forschungssonde = new Ladung();
		Ladung BathlethKlingonenSchwert = new Ladung();
		Ladung PlasmaWaffe = new Ladung();
		Ladung Photonentorpedo = new Ladung();

		FerengiSchneckensaft.setBezeichnung("Ferengi Schneckensaft");
		FerengiSchneckensaft.setMenge(200);

		BorgSchrott.setBezeichnung("Borg-Schrott");
		BorgSchrott.setMenge(5);

		RoteMaterie.setBezeichnung("Rote Materie");
		RoteMaterie.setMenge(2);

		Forschungssonde.setBezeichnung("Forschungssonde");
		Forschungssonde.setMenge(35);

		BathlethKlingonenSchwert.setBezeichnung("Bat'leth Klingonen Schwert");
		BathlethKlingonenSchwert.setMenge(200);

		PlasmaWaffe.setBezeichnung("Plasma-Waffe");
		PlasmaWaffe.setMenge(50);

		Photonentorpedo.setBezeichnung("Photonentorpedo");
		Photonentorpedo.setMenge(3);

		System.out.println("Allgemeines Ladungsverzeichnis: ");
		System.out.println();
		System.out.println("Name der Ladung: " + FerengiSchneckensaft.getBezeichnung());
		System.out.println("Ladungsmenge: " + FerengiSchneckensaft.getMenge());
		System.out.println();
		System.out.println("Name der Ladung: " + BorgSchrott.getBezeichnung());
		System.out.println("Ladungsmenge: " + BorgSchrott.getMenge());
		System.out.println();
		System.out.println("Name der Ladung: " + RoteMaterie.getBezeichnung());
		System.out.println("Ladungsmenge: " + RoteMaterie.getMenge());
		System.out.println();
		System.out.println("Name der Ladung: " + Forschungssonde.getBezeichnung());
		System.out.println("Ladungsmenge: " + Forschungssonde.getMenge());
		System.out.println();
		System.out.println("Name der Ladung: " + BathlethKlingonenSchwert.getBezeichnung());
		System.out.println("Ladungsmenge: " + BathlethKlingonenSchwert.getMenge());
		System.out.println();
		System.out.println("Name der Ladung: " + PlasmaWaffe.getBezeichnung());
		System.out.println("Ladungsmenge: " + PlasmaWaffe.getMenge());
		System.out.println();
		System.out.println("Name der Ladung: " + Photonentorpedo.getBezeichnung());
		System.out.println("Ladungsmenge: " + Photonentorpedo.getMenge());
		System.out.println();

		r3vulkanier.nachrichtAnAlle(null);

		System.out.println("ABSCHUSS DER PHOTONENTORPEDOS!");
		System.out.println();
		System.out.println("Klingonenschiff: ");
		r1klingonen.photonentorpedosSchiessen(r2romulaner);
		System.out.println("Romulanerschiff: ");
		r2romulaner.photonentorpedosSchiessen(r2romulaner);
		System.out.println("Vulkanierschiff: ");
		r3vulkanier.photonentorpedosSchiessen(r3vulkanier);

		System.out.println();

		System.out.println("ABSCHUSS DER PHASERKANONEN!");
		System.out.println();
		System.out.println("Klingonenschiff: ");
		r1klingonen.phaserkanoneSchiessen(r1klingonen);
		System.out.println("Romulanerschiff: ");
		r2romulaner.phaserkanoneSchiessen(r1klingonen);
		System.out.println("Vulkanierschiff: ");
		r3vulkanier.phaserkanoneSchiessen(r3vulkanier);
		System.out.println();

	}
}