import java.util.ArrayList;

public class Raumschiff {

	private String schiffsname;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private static ArrayList<String> broadcastKommunikator;
	private static ArrayList<Ladung> ladungsverzeichnis;

	public Raumschiff() {
		ladungsverzeichnis = new ArrayList<Ladung>();
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;

	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

	public void photonentorpedosSchiessen(Raumschiff r) {
		if (photonentorpedoAnzahl == 0) {
			System.out.println("-=*Click*=-");
		} else {
			photonentorpedoAnzahl = -1;
			System.out.println("Photonentorpedo abgeschossen");
			treffer(r);
		}
	}

	public void phaserkanoneSchiessen(Raumschiff r) {
		if (energieversorgungInProzent < 50) {
			System.out.println("-=*Click*=-");
		} else {
			energieversorgungInProzent = -50;
			System.out.println("Phaserkanone abgeschossen");
			treffer(r);
		}
	}

	public void treffer(Raumschiff r) {
		System.out.println(r.getSchiffsname() + " wurde getroffen!");
		System.out.println();
	}

	public void nachrichtAnAlle(String message) {
		System.out.println("Gewalt ist nicht logisch!!!");
		System.out.println();
	}

	public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;
	}

	public void photonentorpedosLaden(int anzahlTorpedos) {

	}

	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {

	}

	public void zustandRaumschiff(Raumschiff r1klingonen, Raumschiff r2romulaner, Raumschiff r3vulkanier) {

		System.out.println(r1klingonen.getSchiffsname());
		System.out.println(r1klingonen.getAndroidenAnzahl());
		System.out.println(r1klingonen.getLebenserhaltungssystemeInProzent());
		System.out.println(r1klingonen.getHuelleInProzent());
		System.out.println(r1klingonen.getSchildeInProzent());
		System.out.println(r1klingonen.getEnergieversorgungInProzent());
		System.out.println(r1klingonen.getPhotonentorpedoAnzahl());

		System.out.println(r2romulaner.getSchiffsname());
		System.out.println(r2romulaner.getAndroidenAnzahl());
		System.out.println(r2romulaner.getLebenserhaltungssystemeInProzent());
		System.out.println(r2romulaner.getHuelleInProzent());
		System.out.println(r2romulaner.getSchildeInProzent());
		System.out.println(r2romulaner.getEnergieversorgungInProzent());
		System.out.println(r2romulaner.getPhotonentorpedoAnzahl());

		System.out.println(r3vulkanier.getSchiffsname());
		System.out.println(r3vulkanier.getAndroidenAnzahl());
		System.out.println(r3vulkanier.getLebenserhaltungssystemeInProzent());
		System.out.println(r3vulkanier.getHuelleInProzent());
		System.out.println(r3vulkanier.getSchildeInProzent());
		System.out.println(r3vulkanier.getEnergieversorgungInProzent());
		System.out.println(r3vulkanier.getPhotonentorpedoAnzahl());

	}
}
