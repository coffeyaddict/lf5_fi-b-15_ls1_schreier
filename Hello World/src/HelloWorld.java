
public class HelloWorld {

	public static void main(String[] args) {

		System.out.println("Hello World");
		System.out.println("Konsolenausgabe");
		System.out.println("�bung 1");
		System.out.println("Aufgabe 1");
		System.out.println("Guten Tag Phillip\nWillkommen in der Veranstaltung Strukturierte Programmierung.");
		
		System.out.println("Das Wetter ist heute nicht so \"prickelnd\". Vielleicht ist es ja morgen besser.");
		
		System.out.println("Das Wetter ist heute nicht so prickelnd.\nVielleicht ist es ja morgen besser.");
		System.out.println("Aufgabe2");
		//Bei der print()-Anweisung werden die geschriebenen Befehle nacheinander als eine Zeile ausgegeben, ohne einen Zeilenverschub zu erzeugen. Die println()-Anweisung erzeugt einen Zeilenverschub zwischen dem letzten und dem gegenw�rtigen Befehl. 
		
		String s = "*";
		String ss = "***";
		String sss = "*****";
		String ssss = "*******";
		String sssss = "*********";
		String ssssss = "***********";
		String sssssss = "*************";
		String ssssssss = "***";
		String sssssssss = "***";
		System.out.printf( " %7s ", s );
		System.out.printf( "\n %8s \n", ss );
		System.out.printf( " %9s ", sss );
		System.out.printf( "\n %10s \n", ssss );
		System.out.printf( " %11s ", sssss );
		System.out.printf( "\n %12s \n", ssssss );
		System.out.printf( " %s ", sssssss );
		System.out.printf( "\n %8s \n", ssssssss );
		System.out.printf( " %8s ", sssssssss );
		System.out.println("");
		System.out.println("");

		System.out.println("Aufgabe 3");
		double a = 22.4234234;
		System.out.printf( "%.2f \n" ,        a);
		double b = 111.2222;
		System.out.printf( "%.2f \n", 		  b);
		double c = 4.0;
		System.out.printf( "%.2f \n",         c);
		double d = 1000000.551;
		System.out.printf( "%.2f \n",         d);
		double e = 97.34;
		System.out.printf( "%.2f \n",         e);
		System.out.println("");
		System.out.println("Konsolenausgabe");
		System.out.println("�bung 2");
		System.out.println("Aufgabe 1");
		System.out.println(""); 
		System.out.printf(" %5s", "**");
		System.out.printf("\n %s %6s", "*", "*");
		System.out.printf("\n %s %6s", "*", "*");
		System.out.printf("\n %5s", "**");	}

}
