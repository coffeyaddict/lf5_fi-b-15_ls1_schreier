
public class ArtikelTest {

	public static void main(String[] args) 
	{
		Artikel art1 = new Artikel();
		Artikel art2 = new Artikel();
	
		
		
	//Setzen der Attribute
	art1.setName("Himalayareis");
	art1.setArtikelnummer(12);
	art1.setEinkaufspreis(1.41);
	art1.setVerkaufspreis(1.78);
	art1.setMindestbestand(100);
	art1.setLagerbestand(412);
	art2.setName("Frische Fische von Fischers Fritze");
	art2.setArtikelnummer(581);
	art2.setEinkaufspreis(7.21);
	art2.setVerkaufspreis(12.45);
	art2.setMindestbestand(10);
	art2.setLagerbestand(3);
	
	
	//Bildschirmausgaben
	System.out.println("Name des Artikels: " + art1.getName());
	System.out.println("Artikelnummer: " + art1.getArtikelnummer());
	System.out.println("Einkaufspreis: " + art1.getEinkaufspreis() + " �");
	System.out.println("Verkaufspreis: " + art1.getVerkaufspreis() + " �");
	System.out.println("Mindestbestand: " + art1.getMindestbestand());
	System.out.println("Lagerbestand: " + art1.getLagerbestand());
	System.out.println();
	System.out.println("Name des Artikels: " + art2.getName());
	System.out.println("Artikelnummer: " + art2.getArtikelnummer());
	System.out.println("Einkaufspreis: " + art2.getEinkaufspreis() + " �");
	System.out.println("Verkaufspreis: " + art2.getVerkaufspreis() + " �");
	System.out.println("Mindestbestand: " + art2.getMindestbestand());
	System.out.println("Lagerbestand: " + art2.getLagerbestand());
	
	
	}

}
