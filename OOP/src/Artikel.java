
public class Artikel {

	//Attribute
	private String name;
	private int artikelnummer;
	private double einkaufspreis;
	private double verkaufspreis;
	private int mindestbestand;
	
	public Artikel(String name, int artikelnummer, double einkaufspreis, double verkaufspreis, int mindestbestand,
			int lagerbestand) {
		
		this.name = name;
		this.artikelnummer = artikelnummer;
		this.einkaufspreis = einkaufspreis;
		this.verkaufspreis = verkaufspreis;
		this.mindestbestand = mindestbestand;
		this.lagerbestand = lagerbestand;
	}
	public Artikel() {
		
	}
	private int lagerbestand;
	
	//Methoden
	public void setName(String name) {
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	public void setArtikelnummer(int artikelnummer) {
		this.artikelnummer = artikelnummer;
	}
	public int getArtikelnummer() {
		return this.artikelnummer;
	}
	public void setEinkaufspreis(double einkaufspreis) {
		this.einkaufspreis = einkaufspreis;
	}
	public double getEinkaufspreis() {
		return this.einkaufspreis;	
	}
	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}
	public double getVerkaufspreis() {
		return this.verkaufspreis;
	}
	public void setMindestbestand(int mindestbestand) {
		this.mindestbestand = mindestbestand;
	}
	public int getMindestbestand() {
		return this.mindestbestand;
	}
	public void setLagerbestand(int lagerbestand) {
		this.lagerbestand = lagerbestand;
	}
	public int getLagerbestand() {
		return this.lagerbestand;
	}
	
	public void bestelleArtikel() {
		if (mindestbestand < 0.8 * (double) lagerbestand) {
			System.out.println("Artikel wird nachbestellt!");
		}
		else  {
			System.out.println("Artikel ist vorr�tig!");
		}
	}
	public void veraendernLagerbestand() {
		
	}
	public void berechnenGewinnmarge() {
		
	}
	
}
